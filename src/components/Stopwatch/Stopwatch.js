// import { ref } from 'vue';

export class Stopwatch {
	constructor() {
		this.milliseconds   = 0;
		this.is_running     = false;
		this.interval_id    = null;
	}
	
	/**
	 * Преобразует миллисекунды в отформатированную дату
	 * 
	 * @param {number} миллисекудны 
	 * @param {string} формат (например, hh:mm:ss) 
	 * @param {boolean} ignore_zeros (если true, время 00:02:39 преобразуется в 2:39)
	 * 
	 * @returns {string}
	 */
	formatTime(format, ignore_zeros) {
		let formatted_time;
	
		switch(format) {
			case 'hh:mm:ss': 
				formatted_time = new Date(this.milliseconds).toISOString().substring(11, 19);
				break;
			default:
				throw new Error('Неизвестный формат');
		}
	
		if (ignore_zeros) {
			formatted_time = this.delete_zeros(formatted_time);
		}
	
		return formatted_time;
	}
	
	
	/**
	 * Удаляет нули в начале отформатированной строки. Пример: 00:02:02 преобразуется в 2:02
	 * 
	 * @param {string} time – отформатированная строка (например, 00:12:03) 
	 * 
	 * @returns {string} отформатированная строка без нулей в начале
	 */
	delete_zeros(time) {
		return time.replaceAll('00:', '').replace(/^0/, '');
	}
	
	
	/**
	 * Запускает таймер. Обновляется каждые 100 миллисекунд (чтобы при остановке секундомера спустя полсекунды, например, секундомер не начал с 0, игнорируя эти полсекунды)
	 */
	startTimer() {
		if(!this.is_running) {
			this.interval_id = setInterval(() => this.milliseconds += 100, 100);
			this.is_running  = true;
		}
	}
	
	
	/**
	 * Удаляет текущий setInterval
	 */
	stopTimer() {
		clearInterval(this.interval_id);
		this.is_running = false;
	}
	
	
	/**
	 * Удаляет текущий setInterval
	 */
	resetTimer() {
		this.stopTimer();
		this.milliseconds = 0;
	}
}