# stopwatch

Задача: https://docs.google.com/document/d/17WEh-ILvUb8qeT3bdvFQPMuT81EPX85OGGg5M_Q1-uk/edit?usp=sharing <br>
Превью: https://stopwatcher-molodchy.netlify.app/ <br>
Тайм-трекер: 3,5 часов. 
## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```
